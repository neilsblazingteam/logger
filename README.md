# ELK compatible logger

Logger with convenient features (and predefined format)

* indexation
* shared and sharing requestId
* auto-rotation
* timestamps with milliseconds

Example:
```
$logger = Logger::createRotatingFileLogger('file.log', 5, 'Y-m-d');
$logger->notice('Hello world', ['env' => $_SERVER], ['requestIndex' => 'internal-request']);
```
produces file-2017-05-07.log file, which will be removed in 5 days, and adds the NOTICE record to it